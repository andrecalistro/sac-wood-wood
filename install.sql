-- MySQL Workbench Synchronization
-- Generated: 2018-09-26 01:15
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: andre

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `rules` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `email` VARCHAR(255) NULL DEFAULT NULL,
  `password` VARCHAR(255) NULL DEFAULT NULL,
  `rules_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `index2` (`rules_id` ASC),
  CONSTRAINT `fk_users_1`
    FOREIGN KEY (`rules_id`)
    REFERENCES `rules` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `help_desks_statuses` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `areas` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `help_desks` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `users_id` INT(11) NULL DEFAULT NULL,
  `title` VARCHAR(255) NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `help_desks_statuses_id` INT(11) NULL DEFAULT NULL,
  `areas_id` INT(11) NULL DEFAULT NULL,
  `created` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `index2` (`users_id` ASC),
  INDEX `index3` (`help_desks_statuses_id` ASC),
  INDEX `fk_help_desks_3_idx` (`areas_id` ASC),
  CONSTRAINT `fk_help_desks_1`
    FOREIGN KEY (`users_id`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_help_desks_2`
    FOREIGN KEY (`help_desks_statuses_id`)
    REFERENCES `help_desks_statuses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_help_desks_3`
    FOREIGN KEY (`areas_id`)
    REFERENCES `areas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `messages` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `help_desks_id` INT(11) NULL DEFAULT NULL,
  `users_id` INT(11) NULL DEFAULT NULL,
  `message` TEXT NULL DEFAULT NULL,
  `created` DATETIME NULL DEFAULT NULL,
  `type` CHAR(1) NULL DEFAULT NULL COMMENT 'a - attendant\nc - customer',
  PRIMARY KEY (`id`),
  INDEX `index2` (`help_desks_id` ASC),
  INDEX `index3` (`users_id` ASC),
  CONSTRAINT `fk_messages_1`
    FOREIGN KEY (`help_desks_id`)
    REFERENCES `help_desks` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_messages_2`
    FOREIGN KEY (`users_id`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO `rules` (name) VALUES ('Administrador'), ('Cliente');
INSERT INTO `help_desks_statuses` (name) VALUES ('Pendente'), ('Respondido'), ('Finalizado');
INSERT INTO `areas` (name) VALUES ('Comercial'), ('Suporte'), ('Entrega'), ('Outro');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
