<?php

namespace Core;

/**
 * Class Controller
 * @package Core
 */
abstract class Controller
{

    /**
     * @var array
     */
    protected $route_params = [];

    /**
     * Controller constructor.
     * @param $route_params
     */
    public function __construct($route_params)
    {
        $this->route_params = $route_params;
    }

    /**
     * @param $name
     * @param $args
     * @throws \Exception
     */
    public function __call($name, $args)
    {
        $method = $name . 'Action';

        if (method_exists($this, $method)) {
            if ($this->before() !== false) {
                call_user_func_array([$this, $method], $args);
                $this->after();
            }
        } else {
            throw new \Exception("Method $method not found in controller " . get_class($this));
        }
    }

    /**
     *
     */
    protected function before()
    {
    }

    /**
     *
     */
    protected function after()
    {
    }

    /**
     * @param $url
     */
    public function redirect($url)
    {
        $base_url = 'http://' . $_SERVER['SERVER_NAME'];
        header('Location: ' . $base_url . $url, true, 302);
        exit;
    }
}
