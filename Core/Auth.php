<?php

namespace Core;

use PDO;

/**
 * Class Auth
 * @package Core
 */
Class Auth
{
    public $table;
    public $field;
    public $allowed_methods = [];

    /**
     * Auth constructor.
     * @param array $config
     */
    public function __construct(Array $config)
    {
        !isset($config['table']) ?: $this->table = $config['table'];
        !isset($config['field']) ?: $this->field = $config['field'];
        $this->sessionStart();
    }

    /**
     *
     */
    private function sessionStart()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
    }

    /**
     * @param $field
     * @param $password
     * @return mixed
     */
    public function login($field, $password)
    {
        $db = Model::getDB();
        $query = $db->prepare("SELECT * FROM {$this->table} WHERE {$this->field} = :field AND password = :password LIMIT 0,1");
        $query->execute([
            'field' => $field,
            'password' => md5($password)
        ]);
        $result = $query->fetch(PDO::FETCH_ASSOC);
        if (isset($result['password'])) {
            unset($result['password']);
        }
        $_SESSION['User'] = $result;
        return $result;
    }

    /**
     * @return bool
     */
    public function logout()
    {
        if (isset($_SESSION['User'])) {
            unset($_SESSION['User']);
        }
        return true;
    }

    /**
     * @return bool
     */
    public function isLogged()
    {
        if (isset($_SESSION['User']['name']) && !empty($_SESSION['User']['name'])) {
            return true;
        }
        return false;
    }
}