# Bem vindo ao sistema de SAC Wood Wood

Esse sistema foi desenvolvido para o teste da vaga de Programador na empresa Madeira Madeira.

## Como instalar

1. Primeiro, clonar o repositorio.
1. Rodar **composer update** para instalar as dependencias do projeto.
1. Duplicar o arquivo [App/Config.example.php](App/Config.example.php) para App/Config.php.
1. Abrir o arquivo `App/Config.php` e inserir as informações do banco de dados.
1. No navegador rodar a instalação do banco de dados através da url `http://localhost/install`.

## Acessos

Um usuario administrador padrão é criado na instalação:

Email: `admin@woodwood.com.br`
Senha: `123456`

Endereço para acessar o administrador `http://localhost/admin`

## TODO

Alguns pontos que devem ser melhorados, por ordem de prioridade:
* Melhorar o ORM
* Criar sistema de alertas
* Criar sistema de notificações
* Deixar mais inteligente o sistema de rotas
* Relatórios

