<?php

namespace Admin\Models;

use Core\Model;
use PDO;

/**
 * Class HelpDesk
 * @package Admin\Models
 */
Class HelpDesk extends Model
{
    /**
     * @return array
     */
    public static function getList()
    {
        $db = static::getDB();
        $query = $db->query('SELECT a.id, a.title, a.areas_id, a.created, a.help_desks_statuses_id, b.name as name_status, c.name as name_area, d.name as name_user FROM help_desks as a JOIN help_desks_statuses AS b ON b.id = a.help_desks_statuses_id JOIN areas AS c ON c.id = a.areas_id JOIN users AS d ON d.id = a.users_id order by a.created desc');
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function get($id)
    {
        $db = static::getDB();
        $query = $db->prepare('SELECT a.id, a.title, a.description, a.areas_id, a.created, a.help_desks_statuses_id, b.name as name_status, c.name as name_area, d.name as name_user, d.email as email_user FROM help_desks as a JOIN help_desks_statuses AS b ON b.id = a.help_desks_statuses_id JOIN areas AS c ON c.id = a.areas_id JOIN users AS d ON d.id = a.users_id WHERE a.id = :id');
        $query->execute(['id' => $id]);
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param $id
     * @param $help_desks_statuses_id
     * @return bool
     */
    public static function updateStatuses($id, $help_desks_statuses_id)
    {
        $db = static::getDB();
        $query = $db->prepare("UPDATE help_desks SET help_desks_statuses_id = :help_desks_statuses_id WHERE id = :id");
        return $query->execute(['id' => $id, 'help_desks_statuses_id' => $help_desks_statuses_id]);
    }
}