<?php

namespace Admin\Controllers;

use Admin\Models\HelpDesk;
use App\Models\Message;
use Core\View;

class HelpDesks extends \App\Controllers\HelpDesks
{
    public function index()
    {
        $data['items'] = HelpDesk::getList();
        $data['namespace'] = 'Admin';
        View::renderTemplate('HelpDesks/index.html', $data);
    }

    public function view()
    {
        $data['item'] = HelpDesk::get($this->route_params['id']);
        $data['messages'] = Message::getList($this->route_params['id']);
        $data['namespace'] = 'Admin';
        View::renderTemplate('HelpDesks/view.html', $data);
    }

    public function addMessage()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $_POST['help_desks_id'] = $this->route_params['id'];
            $_POST['type'] = 'a';
            $_POST['users_id'] = $_SESSION['User']['id'];
            $_POST['created'] = date('Y-m-d H:i:s');
            Message::save($_POST);

            HelpDesk::updateStatuses($this->route_params['id'], 2);
        }
        $this->redirect('/admin/help-desks/view/' . $this->route_params['id']);
    }
}