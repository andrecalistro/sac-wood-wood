<?php

namespace Admin\Controllers;

use App\Controllers\AppController;
use App\Models\User;
use Core\View;

/**
 * Class Users
 * @package App\Controllers
 */
class Users extends AppController
{
    /**
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function login()
    {
        if ($this->Auth->isLogged() && $_SESSION['User']['rules_id'] == 1) {
            $this->redirect('/help-desks');
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($this->Auth->login($_POST['email'], $_POST['password'])) {
                if ($_SESSION['User']['rules_id'] == 1) {
                    $this->redirect('/admin/help-desks');
                }
                $this->Auth->logout();
                $this->redirect('/admin');
            }
        }
        View::renderTemplate('Users/login.html', ['namespace' => 'Admin']);
    }

    /**
     *
     */
    public function logout()
    {
        $this->Auth->logout();
        $this->redirect('/admin');
    }

    /**
     *
     */
    public function customers()
    {
        $data['items'] = User::getList(2);
        $data['namespace'] = 'Admin';
        View::renderTemplate('Users/customers.html', $data);
    }

    /**
     *
     */
    public function index()
    {
        $data['items'] = User::getList(1);
        $data['namespace'] = 'Admin';
        $data['users_id'] = $_SESSION['User']['id'];
        View::renderTemplate('Users/index.html', $data);
    }

    /**
     *
     */
    public function add()
    {
        $data['namespace'] = 'Admin';
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $_POST['rules_id'] = 1;
            if (User::save($_POST)) {
                $this->redirect('/admin/users');
            }
        }
        View::renderTemplate('Users/add.html', $data);
    }

    /**
     *
     */
    public function delete()
    {
        User::delete($this->route_params['id']);
        $this->redirect('/admin/users');
    }

    /**
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function edit()
    {
        $data['namespace'] = 'Admin';
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['password']) && empty($_POST['password'])) {
                unset($_POST['password']);
            }
            User::save($_POST, $this->route_params['id']);
            $this->redirect('/admin/users');
        }
        $data['item'] = User::get($this->route_params['id']);
        View::renderTemplate('Users/edit.html', $data);
    }
}