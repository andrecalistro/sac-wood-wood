<?php

require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/public/functions.php';

/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');


/**
 * Routing
 */
$router = new Core\Router();

// Add the routes
$router->add('admin', ['namespace' => 'Admin', 'controller' => 'Users', 'action' => 'login']);
$router->add('admin/{controller}', ['namespace' => 'Admin']);
$router->add('admin/{controller}/{action}', ['namespace' => 'Admin']);
$router->add('admin/{controller}/{action}/{id:\d+}', ['namespace' => 'Admin']);

$router->add('', ['controller' => 'Users', 'action' => 'login']);
$router->add('{controller}');
$router->add('{controller}/{action}');
$router->add('{controller}/{action}/{id:\d+}');

$router->dispatch($_SERVER['QUERY_STRING']);