<?php
function dd($var, $stop = true)
{
    echo "<pre>";
    print_r($var);
    echo "</pre>";
    if ($stop)
        die();
}