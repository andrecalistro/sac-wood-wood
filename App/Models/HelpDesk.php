<?php

namespace App\Models;

use PDO;

/**
 * Class HelpDesk
 * @package App\Models
 */
class HelpDesk extends \Core\Model
{
    /**
     * @param null $users_id
     * @return array
     */
    public static function getList($users_id)
    {
        $db = static::getDB();
        $query = $db->prepare('SELECT a.id, a.title, a.areas_id, a.created, a.help_desks_statuses_id, b.name as name_status, c.name as name_area FROM help_desks as a JOIN help_desks_statuses AS b ON b.id = a.help_desks_statuses_id JOIN areas AS c ON c.id = a.areas_id where a.users_id = :users_id order by a.created desc');
        $query->execute(['users_id' => $users_id]);
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function get($id, $users_id)
    {
        $db = static::getDB();
        $query = $db->prepare('SELECT a.id, a.title, a.description, a.areas_id, a.created, a.help_desks_statuses_id, b.name as name_status, c.name as name_area FROM help_desks as a JOIN help_desks_statuses AS b ON b.id = a.help_desks_statuses_id JOIN areas AS c ON c.id = a.areas_id WHERE a.id = :id and a.users_id = :users_id');
        $query->execute(['id' => $id, 'users_id' => $users_id]);
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param $data
     * @param null $id
     * @return string
     */
    public static function save($data, $id = null)
    {
        if ($id) {
            $query = "update help_desks set users_id = :users_id, title = :title, description = :description, help_desks_statuses_id = :help_desks_statuses_id, areas_id = :areas_id where id = :id";
        } else {
            $query = "insert into help_desks (users_id, title, description, help_desks_statuses_id, areas_id, created) values (:users_id, :title, :description, :help_desks_statuses_id, :areas_id, :created)";
        }
        $db = static::getDB();
        $db->prepare($query)
            ->execute($data);
        return $db->lastInsertId();
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $db = static::getDB();
        $query = $db->query('delete FROM help_desks where id = :id');
        $query->bindParam('id', $id);
        return $query->execute();
    }
}
