<?php

namespace App\Models;

use PDO;

/**
 * Class User
 * @package App\Models
 */
class User extends \Core\Model
{
    /**
     * @return array
     */
    public static function getList($rules_id = 2)
    {
        $db = static::getDB();
        $query = $db->prepare('SELECT id, name, email FROM users where rules_id = :rules_id order by name');
        $query->execute(['rules_id' => $rules_id]);
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function get($id)
    {
        $db = static::getDB();
        $stmt = $db->prepare('SELECT id, name, email FROM users where id = :id');
        $stmt->execute(['id' => $id]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param $data
     * @param null $id
     * @return string
     */
    public static function save($data, $id = null)
    {
        if (isset($data['password']) && !empty($data['password'])) {
            $data['password'] = md5($data['password']);
        }
        if ($id) {
            if (isset($data['password']) && !empty($data['password'])) {
                $query = "update users set name = :name, email = :email, password = :password where id = :id";
            } else {
                $query = "update users set name = :name, email = :email where id = :id";
            }
            $data['id'] = $id;
        } else {
            $query = "insert into users (name, email, password, rules_id) values (:name, :email, :password, :rules_id)";
        }
        $db = static::getDB();
        $db->prepare($query)
            ->execute($data);
        return $db->lastInsertId();
    }

    /**
     * @param $id
     * @return bool
     */
    public static function delete($id)
    {
        $db = static::getDB();
        $query = $db->prepare('delete FROM users where id = :id');
        $query->bindParam('id', $id);
        return $query->execute();
    }
}
