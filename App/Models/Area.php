<?php

namespace App\Models;

use PDO;

/**
 * Class Area
 * @package App\Models
 */
class Area extends \Core\Model
{
    /**
     * @return array
     */
    public static function getList()
    {
        $db = static::getDB();
        $query = $db->query('SELECT id, name FROM areas order by name asc');
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
}
