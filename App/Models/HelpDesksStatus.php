<?php

namespace App\Models;

use PDO;

/**
 * Class HelpDesksStatus
 * @package App\Models
 */
class HelpDesksStatus extends \Core\Model
{
    /**
     * @return array
     */
    public static function getList()
    {
        $db = static::getDB();
        $query = $db->query('SELECT id, name FROM help_desks_statuses order by name asc');
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
}
