<?php

namespace App\Models;

use PDO;

/**
 * Class Message
 * @package App\Models
 */
class Message extends \Core\Model
{
    /**
     * @param $help_desks_id
     * @return array
     */
    public static function getList($help_desks_id)
    {
        $db = static::getDB();
        $query = $db->prepare('SELECT a.id, a.message, a.created, a.type, b.name as name_user FROM messages as a JOIN users AS b ON b.id = a.users_id where a.help_desks_id = :id order by a.created desc');
        $query->execute(['id' => $help_desks_id]);
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param $data
     * @param null $id
     * @return string
     */
    public static function save($data, $id = null)
    {
        if ($id) {
            $query = "update messages set message = :message where id = :id";
        } else {
            $query = "insert into messages (help_desks_id, users_id, message, created, type) values (:help_desks_id, :users_id, :message, :created, :type)";
        }
        $db = static::getDB();
        $db->prepare($query)
            ->execute($data);
        return $db->lastInsertId();
    }
}
