<?php

namespace App\Controllers;

use App\Models\User;
use Core\View;

/**
 * Class Users
 * @package App\Controllers
 */
class Users extends AppController
{
    /**
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function register()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $_POST['rules_id'] = 2;
            if (User::save($_POST)) {
                if ($this->Auth->login($_POST['email'], $_POST['password'])) {
                    $this->redirect('/help-desks');
                }
            }
            $this->redirect('/users/login');
        }
        View::renderTemplate('Users/register.html');
    }

    /**
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function login()
    {
        if ($this->Auth->isLogged() && $_SESSION['User']['rules_id'] == 2) {
            $this->redirect('/help-desks');
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($this->Auth->login($_POST['email'], $_POST['password'])) {
                if ($_SESSION['User']['rules_id'] == 2) {
                    $this->redirect('/help-desks');
                }
                $this->Auth->logout();
            }
        }
        View::renderTemplate('Users/login.html');
    }

    /**
     *
     */
    public function logout()
    {
        $this->Auth->logout();
        $this->redirect('/users/login');
    }
}