<?php

namespace App\Controllers;

use Core\Controller;
use Core\Model;
use Core\View;
use PDO;

/**
 * Class Install
 * @package App\Controllers
 */
class Install extends Controller
{
    /**
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function index()
    {
        $message = '<div class="container text-center"><h3>Bem vindo ao sistema de SAC da Wood Wood</h3>';
        $db = Model::getDB();
        $tables = $db->query("SHOW TABLES")->fetchAll(PDO::FETCH_ASSOC);
        if (empty($tables)) {
            $sql = file_get_contents('install.sql');
            $db->query($sql);
            $query = $db->prepare("INSERT INTO users (name, email, password, rules_id) VALUES (:name, :email, :password, :rules_id)");
            $query->execute([
                'name' => 'Admin',
                'email' => 'admin@woodwood.com.br',
                'password' => md5('123456'),
                'rules_id' => 1
            ]);
            $message .= '<p>Banco de dados instalado com sucesso!</p>';
        } else {
            $message .= '<p>Banco de dados já está instalado</p>';
        }
        $message .= '</div>';
        View::renderTemplate('install.html', ['message' => $message]);
    }
}