<?php

namespace App\Controllers;

use App\Models\Area;
use App\Models\HelpDesk;
use App\Models\Message;
use Core\View;

/**
 * Class HelpDesks
 * @package App\Controllers
 */
class HelpDesks extends AppController
{
    /**
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function index()
    {
        $data['items'] = HelpDesk::getList($_SESSION['User']['id']);
        View::renderTemplate('HelpDesks/index.html', $data);
    }

    /**
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $_POST['users_id'] = $_SESSION['User']['id'];
            $_POST['help_desks_statuses_id'] = 1;
            $_POST['created'] = date('Y-m-d H:i:s');
            if (HelpDesk::save($_POST)) {
                $this->redirect('/help-desks');
            }
        }
        $data['areas'] = Area::getList();
        View::renderTemplate('HelpDesks/add.html', $data);
    }

    /**
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function view()
    {
        $data['item'] = HelpDesk::get($this->route_params['id'], $_SESSION['User']['id']);
        $data['messages'] = Message::getList($this->route_params['id']);
        View::renderTemplate('HelpDesks/view.html', $data);
    }

    /**
     *
     */
    public function addMessage()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $_POST['help_desks_id'] = $this->route_params['id'];
            $_POST['type'] = 'c';
            $_POST['users_id'] = $_SESSION['User']['id'];
            $_POST['created'] = date('Y-m-d H:i:s');
            Message::save($_POST);
        }
        $this->redirect('/help-desks/view/' . $this->route_params['id']);
    }
}