<?php

namespace App\Controllers;

use Core\Auth;
use Core\Controller;

/**
 * @property Auth Auth
 */
class AppController extends Controller
{
    /**
     * AppController constructor.
     * @param array $route_params
     */
    public function __construct(array $route_params)
    {
        parent::__construct($route_params);

        $this->Auth = new Auth([
            'table' => 'users',
            'field' => 'email'
        ]);
    }
}